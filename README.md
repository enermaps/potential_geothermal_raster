# potential_geothermal_raster

Input raster file using for computing the shallow energy geothermal potential (conductivity, ground temperature and seasonal length) according to the G.pot methodology (https://www.sciencedirect.com/science/article/pii/S0360544216303358)

## Repository structure

```
datapackage.json                       -- Datapackage JSON file with themain meta-data
data\output_potential_energy.tif       -- Raster with energy potential
data\output_potential_power.tif        -- Raster with power potential
data\input_conductivity.tif            -- Raster with conductivity values
data\input_season_lenght.tif           -- Raster with season lenghts
data\input_input_temperature.tif       -- Raster with ground temperature values
data\input_climate_zones.tif		   -- Raster with climate zones	
```

## Description of the task

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Shallow geothermal</td>
    <td>EU28</td>
    <td>yearly</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

In this task, we collected and re-elaborated data on energy potential of renewable sources at national level, in order to build datasets for all EU28 countries at NUTS3 level. We considered the following renewable sources: biomass, waste and wastewater, shallow geothermal, wind, and solar energy. These data will be used in the toolbox to map the sources of renewable thermal energy across the EU28 and support energy planning and policy.

### Shallow Geothermal Energy

Data show the theorethical energy potential for the Europe computed with [r.green.gshp.theoretical](https://grass.osgeo.org/grass76/manuals/addons/r.green.gshp.theoretical.html).

#### Methodology: ground conductivity

The data showing the ground conductivity are derived from the shape file of [Thermomap project](http://www.eurogeosurveys.org/projects/thermomap/) for the Eusalp region.

#### Methodology: temperature

The land surface temperature, over the EU28, has been used as approximation of the ground temperature for the geothermal energy potential computation. The data can be retrieved from the [climate Hotmaps repository](https://gitlab.com/hotmaps/climate/climate_land_surface_temperature). It is worth noting that this is a strong assumption due to the lack of data about the ground temperature. 

#### Methodology: season heating length and climate zones

The raster with climate zones is derived from the [Commission decision of 1 March 2013](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32013D0114&from=EN) and shows the following spatialized classes:
* 0  Colder climate
* 1  Average climate
* 2  Warmer climate

For computing the shallow geothermal energy and power potential, starting from the Commision decision of 1 March 2013 (Table 1), the following season heating lenght in hours are associated:
* Colder climate, 2470 hours
* Average climate, 2070 hours
* Warmer climate, 1340 hours

The raster data showing the season heating length (expressed in days) is derived considering an average of 10 hours for each day of the heating season. This value was chosen considering the Italian normative on residential thermal plants (art. 9, subsection 2,  DPR 412/93 and subsequent amendments and additions).


## Limitations of data

The data here calculated are estimations of the energy, and power, potential from renewable energy sources. The hypotheses we made when deciding what data to consider, when re-elaborating the data at more aggregated territorial levels and finally when deciding how to convey the results, can influence the results.

In some cases, we underestimated the actual potential (biomass), by downscaling the available resource for sustainability reasons, in others, we overestimated the potential (wind, solar) due to our assumption of using all available areas, according only to some GIS sustainable criteria, where energy generation is feasible without considering economic profitability.

The potentials here reported **do not account for any type of energy transformation**: when estimating the actual potential, the user will need to choose the technology through which the potential can be exploited (for example COP for the wastewater treatment plant or the efficiency for solar thermal, photovoltaic and wind).

For these reasons, the data must be considered **as indicators, rather than absolute figures** representing the actual energy potential of renewable sources in a territory.

## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW) Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

## Authors

Giulia Garegnani<sup>*</sup>,
Antonio Novelli<sup>*</sup>,  
Pietro Zambelli<sup>*</sup>.


<sup>*</sup> Eurac Research 

Institute for Renewable Energy
VoltaStraße/Via Via A. Volta 13/A
39100 Bozen/Bolzano

## License

Copyright © 2016-2018: Giulia Garegnani <giulia.garegnani@eurac.edu>,  Antonio Novelli <antonio.novelli@eurac.edu>, Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
